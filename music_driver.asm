music_driver:
		;clr d0
		bsr test2612
		
        move.b (a0)+,d0
		 cmpi.b #$61,d0
		 beq wait		
	
		 cmpi.b #$66,d0
		 beq loop_playback
		
		cmpi.b #$52,d0 
		 beq update2612_0
		;cmpi.b #$53,d0 
		 ;beq update2612_1
		; cmpi.b #$50,d0
		 ; beq update_psg
		bra music_driver
	
update2612_0:
        move.b (a0)+,$A04000
		;nop
        move.b (a0)+,$A04001
        bra music_driver
		
; update2612_1:	
	    ; move.b (a0)+,$A04002
		; nop
        ; move.b (a0)+,$A04003
		; bra music_driver
		
loop_playback:	
		move.l #$FF0002,a0
		bra music_driver
	
; update_psg:
        ; move.b (a0)+,$C00011
		; bra music_driver
	
wait:
		add.l #$02,a0
		rts

test2612:
		clr d0
        move.b $A04001,d0
		andi.b #$80,d0
        cmpi.b #$80,d0
		beq test2612 
		rts			
   