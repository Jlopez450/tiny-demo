    include "ramdat.asm"
		      dc.l $FFFFFE,   start;,     ErrorTrap, ErrorTrap
	          ; dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
      	      ; dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          ; dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          ; dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap $C0 bytes
              ; dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          ; dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap

setup_vdp:
        lea    $C00004.l,a3     ;VDP control port
	    lea    $C00000.l,a4     ;VDP data port
        lea    (VDPSetupArray),a5
        move.w #0018,d4         ;loop counter
VDP_Loop:
        move.w (a5)+,(a3)       ;load setup array
	    nop
        dbf d4,VDP_Loop
        rts  
vram_Loop:
        move.w (a5)+,(a4)       ;load setup array
        dbf d4,vram_Loop
        rts  
clear_ram:
		move.w #$7FF0,d0
		move.l #$FF0000,a0	
clear_ram_loop:
		move.w #$0000,(a0)+
		dbf d0, clear_ram_loop
		rts
clear_vram:
		move.l #$40000000,(a3)
		move.w #$7fff,d4
vram_clear_loop:
		move.w #$0000,(a4)
		dbf d4, vram_clear_loop
		rts
final_setup:
		move.w #$100,($A11100)
		move.w #$100,($A11200)	
        move.w #$2300, sr       ;enable ints	
		rts
	
ErrorTrap:        
        bra ErrorTrap		

 dc.l HBlank,    ErrorTrap, VBlank ;perfect alignment from here up. Don't add anything!
			  
			  
start:                          
        move.b  $A10001,d0
        andi.b  #$0F,d0
        beq.b   no_tmss       ;branch if oldest MD/GEN
        move.l  #'SEGA',$A14000 ;satisfy the TMSS        
no_tmss:
		move.w #$2700, sr       ;disable ints
		bsr clear_ram
        bsr setup_vdp
		bsr clear_vram
		lea (music),a5
		move.l #$FF0000,a1
		bsr decompress
		move.l #$FF0000,a0
		
		lea (graphic),a5
		move.l #$FF2000,a1
		bsr decompress
		move.l #$FF2000,a5
		move.w #$0120,d4
		move.l #$40200000,(a3)
		bsr vram_loop
		bsr write_message
		clr d0			
		bsr final_setup
loop:
		bra loop

write_message:
		lea (message),a5
		move.l #$67800000,(a3)
		move.w #$002c,d4
		clr d0
msg_loop:	
		move.b (a5)+,d0
		move.w d0,(a4)
		dbf d4,msg_loop
		rts	

	dc.b "SEGA"	;TMSS BS 
	;perfect fit, nothing more above here!
	

VBlank:
		clr d1
		add.w #$0001,a1
		move.w #$0000,d3		
		sub.w #$0001,d2
		move.l #$50000003,(a3);VRAM $D000
		move.w d2,(a4)
		
		bsr scalescript
		bsr music_driver
        rte
			

HBlank:
		sub.w #$01,d1
		move.l #$c0000000,(a3)
		move.w d1,(a4)	
		move.w d7,(a4)
		
		move.w d1,d7
		sub.w a1,d7
		eor.w #$ffff,d7	
		
		add.w #$0001,d3	
		bsr skew_V
		rte	
		
skew_V:
	   move.l #$50000010,(a3) ;write to VSRAM
	   move.w d3,d6
	   muls.w d5,d6
	   lsr #$07,d6
	   sub.w d5,d6	  ;stretch from the middle outward
	   sub.w #$0005,d6        ;center the image better
	   move.w d6,(a4)
	   rts	 
scalescript:
	   tst direction
	    bne reverse

       sub.w #$0001,d5        
	   ;sub.w #$0001,overscroll
	   cmpi.w #$FF00,d5
	    beq noscale
	   rts
noscale:
	   move.b #$ff,direction
	   rts
reverse:	   
       add.w #$0001,d5        
	   ;add.w #$0001,overscroll
	   tst d5
	    beq noscale2
	   rts
noscale2:
	   move.b #$00,direction
	   rts	 	   
		
	include "music_driver.asm"	
	include "decompress.asm"	
		
VDPSetupArray:
	dc.w $8014		
	dc.w $8164  ; Genesis mode, DMA disabled, VBLANK-INT enabled		
	dc.w $8208	;field A    
	dc.w $8300	;$833e	
	dc.w $8402	;field B	
	dc.w $8518	;sprite
	dc.w $8700  ;BG color		
	dc.w $8800
	dc.w $8A00			
	dc.w $8C81	
	dc.w $8D34		
	dc.w $8F02	;auto increment	
	dc.w $9001		
	
message:
	incbin "message.bin"

graphic:
	incbin "text.kos"
	
music:
	incbin "music3.kos"

ROM_End:
              
              